module Main where

import SDLSystems
import GameLogic
import GameTypes
import FRP.Yampa
import FRP.Yampa.Loop
import SDLTimeSense
import PhysicsTest

main :: IO ()
main = do
  sdlState <- initializeSDL
  timeRef <- initializeTimeSense
  reactimate initialInput
             (\_ -> do
               dtSecs <- yampaSDLTimeSense timeRef
               parsedInput <- inputProvider 
               return (dtSecs, Just $ parsedInput {piDt = dtSecs}))
             (\_ gameState -> displayGameState sdlState gameState >> return (getQuitFlag gameState))
             (loopPre initialGameState  gameLogic)