# DinoGame

Clone of 'flappy bird' game implemented in Haskell using:

* sdl2 (sdl2 binding for Haskell)
* Yampa (for functional reactive programming)
* self-made physics engine

# Screenshots
<img src="screenshots/gameInProgress.png" width="600" height="450" />
<img src="screenshots/gameOver.png" width="600" height="450" />

# Build from source
You need [Stack](https://docs.haskellstack.org/en/stable/README/) to build this project.
```
git clone https://gitlab.com/bartekrozanski/dinogame
cd dinogame
stack run
```

# Controls
Up arrow key for force impulse

"q" to quit





