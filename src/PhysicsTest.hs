module PhysicsTest

where

import PhysicsEngine
import GameTypes
import qualified Linear as L
import SDLSystems
import IdSystem
import System.Random

addBox :: Position -> Dimensions -> Coefficient -> ObjId -> PhysicalSystem ->  PhysicalSystem
addBox pos dim@(L.V2 wdth hght) bounciness objId ps = pushCommand ps command
  where
    newObj = PhysicalObject {
      poPos = pos,
      poVel = (L.V2 0 0),
      poAcc = (L.V2 0 0),
      poShape = (Rectangle dim),
      poMass = wdth*hght,
      poBounciness = bounciness,
      poStatic = False,
      poUsingGravity = 1,
      poCollidable = True
    }
    command = AddObject objId newObj


addWall :: Position -> Dimensions -> ObjId -> PhysicalSystem ->  PhysicalSystem
addWall pos dim objId ps = pushCommand ps command
  where
    newObj = PhysicalObject {
      poPos = pos,
      poVel = (L.V2 0 0),
      poAcc = (L.V2 0 0),
      poShape = (Rectangle dim),
      poMass = staticMass,
      poBounciness = 0.0,
      poStatic = True,
      poUsingGravity = 0,
      poCollidable = True
    }
    command = AddObject objId newObj

initialGameStatePhysicsTest :: GameState
initialGameStatePhysicsTest =  iterate addRandomBox GameState {
    getQuitFlag = False,
    getPhysicalSystem = initialPs,
    gsDt = 0,
    gsRandom = randomRs (0,1) (mkStdGen 123),
    gsIdSystem = idSys
  } !! 40
  where
    (playerId, idSys) = getId initIdSystem
    initialPs = (addWall (L.V2 0 0) (L.V2 screenWidth  40) (-4)
      . addWall (L.V2 (screenWidth - 40) 0) (L.V2 40  screenHeight) (-3)
      . addWall (L.V2 0 0) (L.V2 40  screenHeight) (-2) 
      . addBox (L.V2 100 100) (L.V2 40  60) 0.9 playerId
      . addWall (L.V2 0 (screenHeight - 40)) (L.V2 screenWidth  40) (-1)) (emptyPhysicalSystem (L.V2 0 500))


addRandomBox :: GameState -> GameState
addRandomBox gs = newGs
  where
    ((x:y:wdth:hght:vx:vy:[]), newRandom) = splitAt 6 $ gsRandom gs
    xT = stretch (100,500) x
    yT = stretch (100,500) y
    wdthT = stretch (10,40) wdth
    hghtT = stretch (10,40) hght
    vxT = stretch ((-40), 40) vx
    vyT = stretch ((-40), 40) vy
    newBox = PhysicalObject {
      poPos = L.V2 xT yT,
      poVel = L.V2 vxT vyT,
      poAcc = L.V2 0 0,
      poShape = Rectangle $ L.V2 wdthT hghtT,
      poMass = wdthT * hghtT,
      poBounciness = 0.6,
      poStatic = False,
      poUsingGravity = 1,
      poCollidable = True
    }
    ps = getPhysicalSystem gs 
    (newId, newIdSys) = getId $ gsIdSystem gs
    newPs = pushCommand ps (AddObject newId newBox)
    newGs = gs {
      getPhysicalSystem = newPs,
      gsRandom = newRandom,
      gsIdSystem = newIdSys
    } 

