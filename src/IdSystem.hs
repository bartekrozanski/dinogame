module IdSystem

where


type ObjId = Int 
type IdSystem = [ObjId]

initIdSystem :: IdSystem
initIdSystem = [0..]

getId :: IdSystem -> (ObjId, IdSystem)
getId (x:xs) = (x, xs)

returnId :: IdSystem -> ObjId -> IdSystem
returnId sys objId = objId : sys
