module SDLTimeSense
where

import Data.IORef
import qualified SDL
import FRP.Yampa as Yampa

type TimeRef = IORef Int

initializeTimeSense :: IO TimeRef
initializeTimeSense = do
    timeRef <- newIORef (0 :: Int)
    _ <- yampaSDLTimeSense timeRef
    _ <- yampaSDLTimeSense timeRef
    _ <- yampaSDLTimeSense timeRef
    _ <- yampaSDLTimeSense timeRef
    return timeRef


updateTime :: TimeRef -> Int -> IO Int
updateTime timeRef newTime = do
    prevTime <- readIORef timeRef
    writeIORef timeRef newTime
    return (newTime - prevTime)

yampaSDLTimeSense :: TimeRef -> IO Yampa.DTime
yampaSDLTimeSense timeRef = do
    newTime <- fromIntegral <$> SDL.ticks
    dt <- updateTime timeRef newTime
    let dtSecs = fromIntegral dt /1000
    return dtSecs
