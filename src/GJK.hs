module GJK
where

import Linear
import Data.List
import Control.Monad.Trans.State.Strict

type V2D = V2 Double
type Point = V2D
type Direction = V2D
type Radius = Double
type Dimensions = V2D

data Shape = Circle Point Radius | Rectangle Point Dimensions deriving (Show)

type Simplex = [Point]

data GJKResult = FoundIntersection | NoIntersection | InProgress deriving (Eq)

data GJKData = GJKData {
    gjkdSimp :: Simplex,
    gjkdS1 :: Shape,
    gjkdS2 :: Shape,
    gjkdDir :: Direction,
    gjkdRes :: GJKResult
}

type StateGJK a = State GJKData a



getPerpInOriginDir :: Point -> Point -> Direction
getPerpInOriginDir v0 v1 = if v0 `dot` perp1 < 0 then perp1 else perp2  
  where
    vec = v1 - v0
    perp1 = perp vec
    perp2 = negate perp1


minkowskiDiff :: Shape -> Shape -> Direction -> Point
minkowskiDiff s1 s2 dir = (support dir s1) - (support (negate dir) s2)

modifyOnVertexCount :: StateGJK ()
modifyOnVertexCount = do
  state@(GJKData simp s1 s2 dir res) <- get
  let vs = length simp 
  case vs of
    0 -> put $ state {gjkdDir = (centerOfShape s2) - (centerOfShape s1)}
    1 -> put $ state {gjkdDir = negate dir}
    2 -> let v1 = head simp
             v0 = simp !! 1
             in put $ state {gjkdDir = getPerpInOriginDir v0 v1}
    3 -> let v2 = head simp
             v1 = simp !! 1
             v0 = last simp
             v2v1perp = getPerpInOriginDir v1 v2
             v2v0perp = getPerpInOriginDir v0 v2
             v1v0perp = getPerpInOriginDir v1 v0
             in if v2v1perp `dot` (v0) < 0
                  then put $ state {gjkdDir = v2v1perp, gjkdSimp = take 2 simp}
                  else if v2v0perp `dot` (v1) < 0
                         then put $ state {gjkdDir = v2v0perp, gjkdSimp = (head simp) : (last simp) : []}
                         else if v1v0perp `dot` (v2) < 0
                                then  put $ state {gjkdDir = v1v0perp, gjkdSimp = drop 1 simp}
                                else put $ state {gjkdRes = FoundIntersection}
  res <- addSupport
  put $ state {gjkdRes = res}
  return ()


bar :: Shape
bar = Rectangle (V2 0 0) (V2 1 1)

addSupport :: StateGJK GJKResult
addSupport = do
  state@(GJKData simp s1 s2 dir res) <- get
  let newV = minkowskiDiff s1 s2 dir
      result = dir `dot` newV > 0
  if res /= FoundIntersection
    then put (state {gjkdSimp = newV : simp}) >> return (if result then InProgress else NoIntersection)
    else return FoundIntersection

test' :: StateGJK Bool
test' = do
    state@(GJKData simp s1 s2 dir res) <- get
    if res == InProgress
      then modifyOnVertexCount >> test'
      else return (res == FoundIntersection)

test :: Shape -> Shape -> Bool
test s1 s2 = evalState test' (GJKData [] s1 s2 (V2 0 0) InProgress)


(.*) :: Double -> V2D -> V2D
(.*) a (V2 x y) = V2 (a*x) (a*y) 


centerOfShape :: Shape -> Point
centerOfShape (Circle center _ ) = center
centerOfShape (Rectangle topLeft dims) = topLeft + (0.5 .* dims)


support :: Direction -> Shape -> Point
support v (Circle center r) = center + (r .* normalize v)
support v (Rectangle topLeft@(V2 x y) dims@(V2 wdth hght)) = vertices !! maximumIndex 
  where
    vertices = [topLeft, topLeft + (V2 wdth 0), topLeft + (V2 0 hght), topLeft + dims]
    dots = map (dot v) vertices
    (zippedHead:zippedTail) = zip dots [0..]
    (_, maximumIndex) = foldl (\a@(val, ind) b@(nVal, nInd) -> if nVal > val then b else a) zippedHead zippedTail 

