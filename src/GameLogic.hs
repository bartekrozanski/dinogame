{-# LANGUAGE Arrows #-}

module GameLogic
where

import PhysicsEngine
import FRP.Yampa
import FRP.Yampa.Loop
import SDLSystems
import GameTypes
import qualified SDL
import qualified Linear as L
import IdSystem
import HelperArrow
import System.Random
import qualified Data.IntMap.Strict as IntMap


playerId :: ObjId
playerId = 0

initialGameState :: GameState
initialGameState =
  pushGameCommand (AddGameObject $ addPlayer (L.V2 100 100) (L.V2 40 40)) $
  pushGameCommand (AddGameObject $ addRemover (L.V2 (-100) 0) (L.V2 20 screenHeight)) $
  pushGameCommand (AddGameObject $ addSolidWall (L.V2 (-100) (-20)) (L.V2 (screenWidth+200) 60)) $ 
  pushGameCommand (AddGameObject $ addSolidWall (L.V2 (-100) (screenHeight-40)) (L.V2 (screenWidth+200) 60)) $ 
  emptyGs
  where
    emptyGs = GameState {
      getQuitFlag = False,
      getPhysicalSystem = emptyPhysicalSystem (L.V2 0 500),
      gsDt = 0,
      gsRandom = randomRs (0,1) (mkStdGen 123),
      gsIdSystem = initIdSystem,
      gsObjects = IntMap.empty,
      gsCommands = [],
      gsShouldReset = False,
      gsPoints = 0
    }


gameLogic :: SF (ParsedInput, GameState) (GameState, GameState)
gameLogic = second (arr handleResetFlag) >>>
            processInput >>>
            groundFirstInput >>>
            arr processGameCommands >>> 
            applyPhysics >>>
            arr createCommandsFromCollisions >>>
            arr generateObstacles >>>
            splitOutput

handleResetFlag :: GameState -> GameState
handleResetFlag gs = if gsShouldReset gs then initialGameState else gs

processInput :: SF (ParsedInput, GameState) (ParsedInput, GameState)
processInput = quitInputHandler >>> arrowsInputHandler >>> transferDtToGameState


transferDtToGameStatePure :: (ParsedInput, GameState) -> (ParsedInput, GameState)
transferDtToGameStatePure (inp, gs) = (inp, gs {gsDt = correctDt})
  where
    dt = piDt inp
    correctDt = if dt < 0.00001 || dt > 0.5 then 0.0016 else dt
transferDtToGameState :: SF (ParsedInput, GameState)  (ParsedInput, GameState)
transferDtToGameState = arr transferDtToGameStatePure

applyPhysics :: SF GameState GameState
applyPhysics = proc gs -> do
    updatedPs <- updatePhysicalSystem -< (gsDt gs, getPhysicalSystem gs)
    returnA -< gs {getPhysicalSystem = updatedPs}

quitInputHandler :: SF (ParsedInput, GameState) (ParsedInput, GameState)
quitInputHandler = arr quitInputHandlerPure
  where
    quitInputHandlerPure (input, gs)
      | qPressed input = (input, gs {getQuitFlag = True})
      | otherwise = (input, gs)

arrowsInputHandler :: SF (ParsedInput, GameState) (ParsedInput, GameState)
arrowsInputHandler = arr arrowsInputHandler'
  where
    arrowsInputHandler' (input, gs)
      | upPressed input = (input, gs {getPhysicalSystem = pushCommand oldPs (AddVelocity playerId $ L.V2 0 (-250))})
      | otherwise = (input, gs)
      where
        oldPs = getPhysicalSystem gs


generateObstacles :: GameState -> GameState
generateObstacles gs =
  if p < 0.01
    then if q < 0.5
      then pushGameCommand (AddGameObject addRandomTopObstacle ) newGs
      else pushGameCommand (AddGameObject addRandomBotObstacle ) newGs
    else newGs
  where
    (p:q:[], newRandom) = splitAt 2 $ gsRandom gs
    newGs = gs {gsRandom = newRandom}

processGameCommands :: GameState -> GameState
processGameCommands gs = clearGameCommands $ foldl consumeGameCommand gs $ gsCommands gs 

clearGameCommands :: GameState -> GameState
clearGameCommands gs = gs {gsCommands = []}



createCommandsFromCollisions :: GameState -> GameState
createCommandsFromCollisions gs = gs {
  gsCommands = (concatMap (createCommandFromCollision gs) collisions) ++ oldCommands
  }
  where
    collisions = (psCollisionEvents . getPhysicalSystem) gs
    oldCommands = gsCommands gs 
   

createCommandFromCollision :: GameState -> CollisionEvent -> GameCommands
createCommandFromCollision gs (CollisionEvent id1 id2 _) =
  collisionToGameCommand obj1 obj2 id1 id2
    where
      objs = gsObjects gs
      obj1 = objs IntMap.! id1
      obj2 = objs IntMap.! id2
    
collisionToGameCommand :: GameObject -> GameObject -> ObjId -> ObjId -> GameCommands
collisionToGameCommand Remover _ _ id2 = [RemoveGameObject id2, AddPoints 1]
collisionToGameCommand _ Remover id1 _ = [RemoveGameObject id1, AddPoints 1]
collisionToGameCommand Player Obstacle _ _ = pure TriggerResetFlag
collisionToGameCommand Obstacle Player _ _ = pure TriggerResetFlag
collisionToGameCommand SolidWall Player _ _ = pure TriggerResetFlag
collisionToGameCommand Player SolidWall _ _ = pure TriggerResetFlag
collisionToGameCommand _ _ _ _ = [] 


pushGameCommand :: GameCommand -> GameState -> GameState
pushGameCommand gc gs = gs {gsCommands =  gc: oldCommands}
  where
    oldCommands = gsCommands gs

consumeGameCommand :: GameState -> GameCommand -> GameState
consumeGameCommand gs gc = 
  case gc of
    RemoveGameObject objId ->
      removeGameObj objId gs
    AddGameObject addingFunc ->
      addingFunc gs
    TriggerResetFlag ->
      gs {gsShouldReset = True}
    AddPoints points ->
      gs {gsPoints = currentPoints + points}
  where
    objects = gsObjects gs
    currentPoints = gsPoints gs 

removeGameObj :: ObjId -> GameState -> GameState
removeGameObj objId gs = gs {
  getPhysicalSystem = pushCommand ps (RemoveObject objId),
  gsObjects = IntMap.delete objId $ gsObjects gs,
  gsIdSystem = newIdSys
  } 
  where
    ps = getPhysicalSystem gs
    newIdSys = returnId (gsIdSystem gs) objId

addRemover :: Position -> Dimensions -> GameState -> GameState
addRemover pos dims gs = gs {
  gsIdSystem = newIdSys,
  getPhysicalSystem = pushCommand ps (AddObject newId newPo),
  gsObjects = IntMap.insert newId newGameObj $ gsObjects gs
  } 
  where
    newPo = PhysicalObject {
      poPos = pos,
      poVel = pure 0,
      poAcc = pure 0,
      poShape = Rectangle dims,
      poMass = 1,
      poBounciness = 0.4,
      poStatic = True,
      poUsingGravity = 0,
      poCollidable = True
    }
    (newId, newIdSys) = getId $ gsIdSystem gs
    ps = getPhysicalSystem gs
    newGameObj = Remover

addRandomTopObstacle :: GameState -> GameState
addRandomTopObstacle gs = newGs {gsRandom = newRandom}
  where
    (wdth:hght:[],newRandom) = splitAt 2 $ gsRandom gs
    wdthT = stretch (30,75) wdth
    hghtT = stretch (60,280) hght
    newGs = addObstacle (L.V2 screenWidth (100)) (L.V2 wdthT hghtT) (-1) gs 

addRandomBotObstacle :: GameState -> GameState
addRandomBotObstacle gs = newGs {gsRandom = newRandom}
  where
    (wdth:hght:[],newRandom) = splitAt 2 $ gsRandom gs
    wdthT = stretch (30,75) wdth
    hghtT = stretch (60,280) hght
    newGs = addObstacle (L.V2 screenWidth (screenHeight - hghtT - 100)) (L.V2 wdthT hghtT) 1 gs


addObstacle :: Position -> Dimensions -> Double -> GameState -> GameState
addObstacle pos dims grav gs = gs {
  gsIdSystem = newIdSys,
  getPhysicalSystem = pushCommand ps (AddObject newId newPo),
  gsObjects = IntMap.insert newId newGameObj $ gsObjects gs
  } 
  where
    newPo = PhysicalObject {
      poPos = pos,
      poVel = (L.V2 (-150) 0),
      poAcc = pure 0,
      poShape = Rectangle dims,
      poMass = 1,
      poBounciness = 0.2,
      poStatic = False,
      poUsingGravity = grav,
      poCollidable = True
    }
    (newId, newIdSys) = getId $ gsIdSystem gs
    ps = getPhysicalSystem gs
    newGameObj = Obstacle

addPlayer :: Position -> Dimensions -> GameState -> GameState
addPlayer pos dims gs = gs {
  gsIdSystem = newIdSys,
  getPhysicalSystem = pushCommand ps (AddObject newId newPo),
  gsObjects = IntMap.insert newId newGameObj $ gsObjects gs
  } 
  where
    newPo = PhysicalObject {
      poPos = pos,
      poVel = pure 0,
      poAcc = pure 0,
      poShape = Rectangle dims,
      poMass = 1,
      poBounciness = 0.4,
      poStatic = False,
      poUsingGravity = 1,
      poCollidable = True
    }
    (newId, newIdSys) = getId $ gsIdSystem gs
    ps = getPhysicalSystem gs
    newGameObj = Player
 

addSolidWall :: Position -> Dimensions ->  GameState -> GameState
addSolidWall pos dims gs = gs {
  gsIdSystem = newIdSys,
  getPhysicalSystem = pushCommand ps (AddObject newId newPo),
  gsObjects = IntMap.insert newId newGameObj $ gsObjects gs
} 
  where
    newPo = PhysicalObject {
      poPos = pos,
      poVel = pure 0,
      poAcc = pure 0,
      poShape = Rectangle dims,
      poMass = staticMass,
      poBounciness = 1,
      poStatic = True,
      poUsingGravity = 0,
      poCollidable = True
    }
    (newId, newIdSys) = getId $ gsIdSystem gs
    ps = getPhysicalSystem gs
    newGameObj = SolidWall