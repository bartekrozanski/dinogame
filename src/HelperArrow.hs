{-# LANGUAGE Arrows #-}
module HelperArrow
where

import FRP.Yampa

groundFirstInput :: SF (a,b) b
groundFirstInput = arr snd

splitOutput :: SF a (a,a)
splitOutput = identity &&& identity