{-# LANGUAGE Arrows #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib
where

import FRP.Yampa
import Control.Concurrent
import SDL (($=))
import qualified SDL
import Control.Monad (unless)
import Data.Text hiding (any, take)
import Foreign.C.Types
import FRP.Yampa.Loop
import Box
type Pos = Double
type Vel = Double
type Temp = Double



data Player = Player {getBox :: Box}

data GameState = GameState {
    getPlayer :: Player
}

data ParsedInput = ParsedInput{
    quitClicked :: Bool
}

data SDLState = SDLState{
    getWindow :: SDL.Window,
    getRenderer :: SDL.Renderer
}

fallingBall :: Pos -> SF () (Pos, Vel)
fallingBall y0 = constant y0 &&& constant 0




coolingDown :: Temp -> SF Temp Temp
coolingDown t0 = constant (-1) >>> integral >>^ (+t0)

coolingDownWithFloor :: Temp -> Temp -> SF () Temp
coolingDownWithFloor t0 tf = switch coolDown' floorFunc
  where 
    coolDown' = proc _ -> do
        t <- (+ t0) ^<< integral -< (-1)
        e <- edge -< t <= tf
        returnA -< (t, e `tag` t)
    floorFunc = constant


initialGameState :: GameState
initialGameState = GameState {getPlayer = Player (Box 10 20 100 100)}

mainn :: IO ()
mainn = do
  sdlState <- initializeSDL
  reactimate (return $ ParsedInput {quitClicked = False})
                          (\_ -> return (0.1, Just (ParsedInput {quitClicked = False})))
                          (\_ gameState -> displayGameState sdlState gameState >> return False)
                          (loopPre initialGameState gameLogic) 


gameLogic :: SF (ParsedInput, GameState) (GameState, GameState)
gameLogic = proc _ -> do
    let gs = GameState {getPlayer = Player (Box 40 100 200 200)}
    returnA -< (gs, gs)


drawBox :: SDL.Renderer -> Box -> IO ()
drawBox rend box = SDL.drawRect rend $ Just (boxToRect box)

fillRect :: SDL.Renderer -> Int -> Int -> Int -> Int -> IO ()
fillRect rend left top width height = 
    SDL.fillRect rend (Just (SDL.Rectangle (SDL.P $ SDL.V2 cleft ctop) (SDL.V2 cwidth cheight)))
    where
        cleft = fromIntegral left
        ctop = fromIntegral top
        cwidth = fromIntegral width
        cheight = fromIntegral height



screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (800, 600)


gameTitle :: Text
gameTitle = "Flappy Bat"

initializeSDL :: IO SDLState
initializeSDL = do
    SDL.initializeAll
    window <- SDL.createWindow gameTitle SDL.defaultWindow {SDL.windowInitialSize = SDL.V2 screenWidth screenHeight}
    SDL.showWindow window
    renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
    return $ SDLState {getWindow = window, getRenderer = renderer}


inputProvider :: IO ParsedInput
inputProvider = do
  events <- SDL.pollEvents
  let eventIsQPress event =
        case SDL.eventPayload event of
          SDL.KeyboardEvent keyboardEvent ->
            SDL.keyboardEventKeyMotion keyboardEvent == SDL.Pressed &&
            SDL.keysymKeycode (SDL.keyboardEventKeysym keyboardEvent) == SDL.KeycodeQ
          _ -> False
      qPressed = any eventIsQPress events
  return (ParsedInput {quitClicked = qPressed})

displayGameState :: SDLState -> GameState -> IO ()
displayGameState sdls gs = do
    renderer <- return $ getRenderer sdls
    player <- return $ getPlayer gs
    SDL.rendererDrawColor renderer $= SDL.V4 0 0 255 255
    SDL.clear renderer
    SDL.rendererDrawColor renderer $= SDL.V4 0 255 255 255
    fillRect renderer 10 10 200 100
    SDL.present renderer
    return ()