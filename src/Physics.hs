{-# LANGUAGE Arrows #-}

-- necessery to create an instance of VectorSpace for SDL.V2
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Physics
where

import qualified SDL
import Foreign.C.Types
import GameTypes
import FRP.Yampa


-- integral function from Yampa requires input to be an instance of VectorSpace
instance VectorSpace (SDL.V2 Double) Double where
  zeroVector = SDL.V2 0 0
  (*^) a (SDL.V2 x y) = SDL.V2 (x*a) (y*a)
  (^/) (SDL.V2 x y) a = SDL.V2 (x/a) (y/a)
  (^+^) v u = v + u
  negateVector v = negate v
  dot (SDL.V2 x0 y0) (SDL.V2 x1 y1) = x0*x1 + y0*y1
  norm (SDL.V2 x y) = x^2 + y^2
  normalize v = v ^/ (norm v)




checkCollision :: PhysicalObject -> PhysicalObject -> Bool
checkCollision = undefined

changeCenter :: Double -> Double -> Box -> Box
changeCenter x y (Box oldX oldY width height) = Box x y width height

translateCenter :: Double -> Double -> Box -> Box
translateCenter x y (Box oldX oldY width height) = Box (x+oldX) (y+oldY) width height


getLeftTop :: Box -> (Double, Double)
getLeftTop (Box centerX centerY width height) = (centerX - (width/2), centerY - (height/2))

boxToRect :: Box -> SDL.Rectangle CInt
boxToRect box = SDL.Rectangle (SDL.P $ SDL.V2 cleft ctop) (SDL.V2 cwidth cheight)
  where
    trans = fromIntegral . round
    (left, top) = getLeftTop box
    cleft = trans left
    ctop = trans top
    cheight = trans $ getHeight box
    cwidth = trans $ getWidth box


collideBoxes :: Box -> Box -> (Collision, Collision)
collideBoxes box1@(Box _ _ width1 height1) box2@(Box _ _ width2 height2) =
  if xMin1 < xMax2 && xMax1 > xMin2 && yMin1 < yMax2 && yMax1 > yMin2
    then (Collision, Collision)
    else (NoCollision, NoCollision)
  where
    (xMin1, yMin1) = getLeftTop box1
    (xMin2, yMin2) = getLeftTop box2
    (xMax1, yMax1) =  (xMin1 + width1, yMin1 + height1)
    (xMax2, yMax2) =  (xMin2 + width2, yMin2 + height2)


addVel :: Velocity -> PhysicalObject -> PhysicalObject
addVel vec object = object {getVel = getVel object + vec}

setVel :: Velocity -> PhysicalObject -> PhysicalObject
setVel vec object = object {getVel = vec}


accIntegral :: SF PhysicalObject PhysicalObject
accIntegral = proc (PhysicalObject box vel acc objType) -> do
  integrated <- integral -< acc
  returnA -< (PhysicalObject box (integrated + vel) acc objType)

velIntegral :: SF PhysicalObject PhysicalObject
velIntegral = proc (PhysicalObject box vel acc objType) -> do
  (SDL.V2 x y) <- integral -< vel
  returnA -< (PhysicalObject (translateCenter x y box) vel acc objType)

dynamicArr :: SF PhysicalObject PhysicalObject
dynamicArr = accIntegral >>> velIntegral




collide :: (PhysicalObject, PhysicalObject) -> (PhysicalObject, PhysicalObject)
collide inp@(obj1@(PhysicalObject box1 vel1 acc1 type1), obj2@(PhysicalObject box2 vel2 acc2 type2)) =
  case collideBoxes box1 box2 of
    (Collision, Collision) -> (setVel (SDL.V2 0 (-1)) obj1, setVel zeroVector obj2)
    _ -> inp
