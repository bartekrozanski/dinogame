module GameTypes
where
import qualified SDL
import PhysicsEngine
import IdSystem
import System.Random
import Control.Monad.Trans.State
import Control.Monad
import qualified Data.IntMap.Strict as IntMap

data GameObject = Obstacle | Player | SolidWall | Remover  
data GameCommand = RemoveGameObject ObjId
  | AddGameObject (GameState -> GameState)
  | TriggerResetFlag
  | AddPoints Int

type GameObjects = IntMap.IntMap GameObject
type GameCommands = [GameCommand]


data GameState = GameState {
  getQuitFlag :: Bool,
  getPhysicalSystem :: PhysicalSystem,
  gsDt :: Double,
  gsRandom :: [Double],
  gsIdSystem :: IdSystem,
  gsObjects :: GameObjects,
  gsCommands :: GameCommands,
  gsShouldReset :: Bool,
  gsPoints :: Int
}

data ParsedInput = ParsedInput{
    piDt :: Double,    
    oPressed :: Bool,
    qPressed :: Bool,
    upPressed :: Bool,
    downPressed :: Bool,
    leftPressed :: Bool,
    rightPressed :: Bool
}

stretch :: (Double, Double) -> Double -> Double
stretch (minVal, maxVal) v = ((maxVal - minVal) * v) + minVal