{-# LANGUAGE Arrows #-}


module PhysicsEngine
where

import Data.Ord
import Data.List
import Data.Maybe


import FRP.Yampa
import Linear
import qualified Data.IntMap.Strict as IntMap
import IdSystem
import HelperArrow

type V2D = V2 Double
type Position = V2D
type Velocity = V2D
type Acceleration = V2D
type Dimensions = V2D
type Mass = Double
type PhysicalObjects = IntMap.IntMap PhysicalObject
type TimeDelta = Double
type PenetrationVec = V2D
type Coefficient = Double

type EngineCommands = [EngineCommand]
data EngineCommand = OverriteVelocity ObjId V2D
  | AddVelocity ObjId V2D
  | OverriteAcceleration ObjId V2D
  | AddObject ObjId PhysicalObject
  | RemoveObject ObjId


data CollisionEvent = CollisionEvent ObjId ObjId PenetrationVec deriving(Show)
type CollisionEvents = [CollisionEvent]

data Shape = Rectangle Dimensions
  | Circle Double

-- type CollisionEvent = (ObjInt, ObjInt)
-- type CollisionEvents = [CollisionEvent]

data PhysicalObject = PhysicalObject {
    poPos :: Position,
    poVel :: Velocity,
    poAcc :: Acceleration,
    poShape :: Shape,
    poMass :: Mass,
    poBounciness :: Double,
    poStatic :: Bool,
    poUsingGravity :: Double,
    poCollidable :: Bool
}

data PhysicalSystem = PhysicalSystem {
    psCollisionEvents :: CollisionEvents,
    psCommands :: EngineCommands,
    psObjects :: PhysicalObjects,
    psG :: Acceleration
}

updatePhysicalSystem :: SF (TimeDelta, PhysicalSystem) PhysicalSystem
updatePhysicalSystem =
    second (arr clearCollisionEvents) >>>
    second (arr processEngineCommands) >>>
    arr applyGravityToSystem >>>
    arr integratePhysicalObjects >>>
    second (arr generateCollisions) >>>
    second (arr reactToCollisionEvents) >>>
    groundFirstInput

(.*) :: Double -> V2D -> V2D
(.*) a (V2 x y) = V2 (a*x) (a*y)



mirrorVec :: V2D -> V2D -> V2D
mirrorVec n' d = d - ((2 * (d `Linear.dot` n)) .* n)
  where
    n = Linear.normalize n'

pushCommand :: PhysicalSystem -> EngineCommand -> PhysicalSystem
pushCommand ps ec = ps {psCommands = newCommands}
  where
    oldCommands = psCommands ps
    newCommands = ec : oldCommands

emptyPhysicalSystem :: Acceleration -> PhysicalSystem
emptyPhysicalSystem acc = PhysicalSystem {
    psCollisionEvents = [],
    psCommands = [],
    psObjects = IntMap.empty,
    psG = acc
}


consumeCommand :: PhysicalSystem -> EngineCommand -> PhysicalSystem
consumeCommand ps ec = case ec of
  (OverriteVelocity objId vec) ->
    ps {psObjects = (IntMap.adjust (\po -> po {poVel = vec}) objId oldMap)}
  (AddVelocity objId vec) ->
    ps {psObjects = (IntMap.adjust (\po -> po {poVel = poVel po + vec}) objId oldMap)}
  (OverriteAcceleration objId vec) ->
    ps {psObjects = (IntMap.adjust (\po -> po {poAcc = vec}) objId oldMap)}
  (AddObject objId po) ->
    ps {psObjects = (IntMap.insert objId po oldMap)}
  (RemoveObject objId) ->
    ps {psObjects = (IntMap.delete objId oldMap)}
  where
    oldMap = psObjects ps

clearEngineCommands :: PhysicalSystem -> PhysicalSystem
clearEngineCommands ps = ps {psCommands = []}

processEngineCommands :: PhysicalSystem -> PhysicalSystem
processEngineCommands ps = clearEngineCommands $ foldl consumeCommand ps (psCommands ps)

integratePhysicalObjects :: (TimeDelta, PhysicalSystem) -> (TimeDelta, PhysicalSystem)
integratePhysicalObjects (dt, ps) = (dt, ps {psObjects = newObjs})
  where
    oldObjs = psObjects ps
    newObjs = IntMap.map (integratePhysicalObject dt) oldObjs

applyGravity :: Acceleration -> TimeDelta -> PhysicalObject -> PhysicalObject
applyGravity g dt po = po {poVel = (gravityCoef .* (dt .* g)) + poVel po}
  where
    gravityCoef = poUsingGravity po

applyGravityToSystem :: (TimeDelta, PhysicalSystem) -> (TimeDelta, PhysicalSystem)
applyGravityToSystem (dt, ps) = (dt, ps {
    psObjects = IntMap.map (applyGravity g dt) oldMap
  })
  where
    g = psG ps
    oldMap = psObjects ps

integratePhysicalObject :: TimeDelta -> PhysicalObject -> PhysicalObject
integratePhysicalObject dt po
  | poStatic po = po
  | otherwise = po {poPos = newP, poVel = newV}
  where
    acc0 = poAcc po
    vel0 = poVel po
    pos0 = poPos po
    newV = (dt .* acc0) + vel0
    newP = (dt .* newV) + pos0
-----------------------------------
--
--
-- Collision Handling related code
--
--
------------------------------------
staticMass :: Double
staticMass = 99999999

shapeCollision :: Position -> Shape -> Position -> Shape -> Maybe PenetrationVec
shapeCollision (V2 x1 y1) (Rectangle (V2 w1 h1)) (V2 x2 y2) (Rectangle (V2 w2 h2)) =
  if collides
    then Just closest
    else Nothing
  where
    left = x1-x2-w2
    top = y2 - y1 - h1
    wdth = w1 + w2
    hght = h1 + h2
    bot = top + hght
    right = left + wdth
    collides = left <= 0 && right >= 0 && bot >= 0 && top <= 0
    closest = minimumBy (\a b -> compare (Linear.norm a) (Linear.norm b)) [V2 (-right) 0, V2 (-left) 0, V2 0 (top), V2 0 (bot)]
shapeCollision _ _ _ _ = undefined


shouldCalculateCollision :: PhysicalObject -> PhysicalObject -> Bool
shouldCalculateCollision (PhysicalObject _ _ _ _ _  _ static1 _ collidable1) (PhysicalObject _ _  _ _ _ _ static2 _ collidable2)
  | static1 && static2 = False
  | collidable1 && collidable2 = True
  | otherwise = False

generateCollision :: PhysicalObjects -> ObjId -> ObjId -> CollisionEvents
generateCollision objs id1 id2
  | shouldCalculateCollision po1 po2 =
      case shapeCollision pos1 s1 pos2 s2 of
        Just penVec -> [CollisionEvent id1 id2 penVec]
        Nothing -> []
  | otherwise = []
  where
    po1@(PhysicalObject pos1 vel1 _ s1 m1 _ _ _ _) = fromJust $ IntMap.lookup id1 objs
    po2@(PhysicalObject pos2 vel2 _ s2 m2 _ _ _ _) = fromJust $ IntMap.lookup id2 objs

generateCollisions :: PhysicalSystem -> PhysicalSystem
generateCollisions ps = ps {psCollisionEvents = collisions}
  where
    objs = psObjects ps
    pairs = (generateUniquePairs . IntMap.keys) objs
    collisions = concatMap (uncurry (generateCollision objs)) pairs

processCollisionEvent :: PhysicalSystem -> CollisionEvent -> PhysicalSystem
processCollisionEvent ps ce = ps {psObjects = newObjs}
  where
    objs = psObjects ps
    newObjs = reactToCollision objs ce


clearCollisionEvents :: PhysicalSystem -> PhysicalSystem
clearCollisionEvents ps = ps {psCollisionEvents = []}

reactToCollisionEvents :: PhysicalSystem -> PhysicalSystem
reactToCollisionEvents ps = foldl processCollisionEvent ps events
  where
    events = psCollisionEvents ps

generateUniquePairs :: (Ord a) => [a] -> [(a, a)]
generateUniquePairs l = filter (uncurry (<)) $ l >>= (\el1 -> l >>= (\el2 -> [(el1,el2)]))

reactToCollision :: PhysicalObjects -> CollisionEvent -> PhysicalObjects
reactToCollision objs ce@(CollisionEvent id1 id2 penVec) = newObjs2
  where
    po1 = objs IntMap.! id1
    po2 = objs IntMap.! id2
    vel1 = poVel po1
    m1 = poMass po1
    cr1 = poBounciness po1
    vel2 = poVel po2
    m2 = poMass po2
    cr2 = poBounciness po2
    (paral1, perp1) = splitToPerpComponents penVec vel1
    (paral2, perp2) = splitToPerpComponents penVec vel2
    (newParal1, newParal2) = calculateVelsAfterCollision paral1 m1 cr1 paral2 m2 cr2
    adjustFunc :: Velocity -> V2D -> PhysicalObject -> PhysicalObject
    adjustFunc newVel offsetVec po =
      if poStatic po
        then po
        else po {poVel = newVel, poPos = poPos po + offsetVec}
    newObjs1 = IntMap.adjust (adjustFunc (newParal1 + perp1) penVec) id1 objs
    newObjs2 = IntMap.adjust (adjustFunc (newParal2 + perp2) $ negate penVec) id2 newObjs1

calculateVelsAfterCollision :: Velocity -> Mass -> Coefficient -> Velocity -> Mass -> Coefficient -> (Velocity, Velocity)
calculateVelsAfterCollision v1 m1 cr1 v2 m2 cr2 = (u1, u2)
  where
    u1 = (1/(m1 + m2)) .* ((cr1 * m2) .* (v2-v1) + (m1 .* v1) + (m2 .* v2))
    u2 = (1/(m1 + m2)) .* ((cr2 * m1) .* (v1-v2) + (m1 .* v1) + (m2 .* v2))


angleBetweenDirAndVec :: V2D -> V2D -> Double
angleBetweenDirAndVec dir v = acos cosAlfa
  where
    normDir = Linear.norm dir
    normV = Linear.norm v
    cosAlfa = abs ((dir `Linear.dot` v)/(normDir * normV))

splitToPerpComponents :: V2D -> V2D -> (V2D, V2D)
splitToPerpComponents dir vec0 = (parallel, perpendicular)
  where
    normDir = Linear.norm dir
    s = (dir `Linear.dot` vec0)/(normDir ^ 2)
    parallel = s .* dir
    perpendicular = vec0 - parallel
