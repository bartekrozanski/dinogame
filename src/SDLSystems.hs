{-# LANGUAGE OverloadedStrings #-}


module SDLSystems
where


import SDL (($=))
import qualified SDL
import Foreign.C.Types
import PhysicsEngine
import GameTypes
import qualified Linear as L
import Data.Text hiding (any)
import Control.Monad
import Data.IntMap.Strict as IntMap
import Data.Map.Strict as Map
import IdSystem
import System.Directory
import System.IO



highscoreFile :: FilePath
highscoreFile = "highscore"




data TextureLabel = SolidWallTopTexture
  | SolidWallBotTexture
  | ObstacleTopTexture
  | ObstacleBotTexture
  | PlayerTexture deriving (Eq, Ord)

type Textures = Map.Map TextureLabel SDL.Texture

data SDLState = SDLState{
    getWindow :: SDL.Window,
    getRenderer :: SDL.Renderer,
    getTextures :: Textures
}


screenWidth :: (Num a) => a
screenWidth = 800

screenHeight :: (Num a) => a
screenHeight = 600

gameTitle :: Text
gameTitle = "Dino Game"

initialInput :: IO ParsedInput
initialInput = return $ ParsedInput 0 False False False False False False


loadTextureFromFile :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadTextureFromFile rend path = do
  surface <- SDL.loadBMP path
  texture <- SDL.createTextureFromSurface rend surface
  SDL.freeSurface surface
  return texture

addToTextures :: SDL.Renderer -> Textures -> (FilePath, TextureLabel) -> IO Textures
addToTextures rend textures (path,label) = do
  texture <- loadTextureFromFile rend path
  return $ Map.insert label texture textures

initializeSDL :: IO SDLState
initializeSDL = do
    SDL.initializeAll
    window <- SDL.createWindow gameTitle SDL.defaultWindow {SDL.windowInitialSize = SDL.V2 screenWidth screenHeight}
    SDL.showWindow window
    renderer <- SDL.createRenderer window (-1) config
    textures <-
      foldM (addToTextures renderer) Map.empty loadList
    return $ SDLState {getWindow = window, getRenderer = renderer, getTextures = textures}
    where
      loadList = [("./wallBot.bmp", SolidWallBotTexture),
        ("./wallTop.bmp", SolidWallTopTexture),
        ("./bat.bmp", PlayerTexture),
        ("./stalactite.bmp", ObstacleTopTexture),
        ("./stalagnate.bmp", ObstacleBotTexture)]
      config = SDL.RendererConfig {SDL.rendererType = SDL.AcceleratedVSyncRenderer, SDL.rendererTargetTexture = False}


inputProvider :: IO ParsedInput
inputProvider = do
  events <- SDL.pollEvents
  return (ParsedInput {
    oPressed = oKeyPressed events,
    qPressed = qKeyPressed events,
    upPressed = upKeyPressed events,
    downPressed = downKeyPressed events,
    leftPressed = leftKeyPressed events,
    rightPressed = rightKeyPressed events
    })
  where
    eventIsKeyPressed keyCode event =
      case SDL.eventPayload event of
        SDL.KeyboardEvent keyboardEvent ->
          SDL.keyboardEventKeyMotion keyboardEvent == SDL.Pressed &&
          SDL.keysymKeycode (SDL.keyboardEventKeysym keyboardEvent) == keyCode
        _ -> False
    isKeyPressed keycode = any (eventIsKeyPressed keycode)
    qKeyPressed = isKeyPressed SDL.KeycodeQ
    oKeyPressed = isKeyPressed SDL.KeycodeO
    upKeyPressed = isKeyPressed SDL.KeycodeUp
    downKeyPressed = isKeyPressed SDL.KeycodeDown
    leftKeyPressed = isKeyPressed SDL.KeycodeLeft
    rightKeyPressed = isKeyPressed SDL.KeycodeRight


getHighScore :: IO Int
getHighScore = do
  exists <- doesFileExist highscoreFile 
  if exists
    then withFile highscoreFile ReadMode readFunc
    else return 0
  where
    readFunc h = do
      line <- hGetLine h 
      return $ read line

saveScore :: Int -> IO ()
saveScore points = do
  withFile highscoreFile WriteMode (\h -> hPutStrLn h (show points))



displayGameState :: SDLState -> GameState -> IO ()
displayGameState sdls gs = do
    renderer <- return $ getRenderer sdls
    textures <- return $ getTextures sdls
    window <- return $ getWindow sdls
    SDL.rendererDrawColor renderer $= SDL.V4 110 82 41 255
    SDL.clear renderer
    SDL.rendererDrawColor renderer $= SDL.V4 0 255 255 255
    mapM_ (renderGameObject renderer textures gs) ((IntMap.keys . gsObjects) gs)
    SDL.present renderer
    SDL.windowTitle window $= pack ("FlappyBat: " ++ show currentPoints)
    if gsShouldReset gs
      then do
        highscore <- getHighScore
        if highscore < currentPoints
          then do
            SDL.showSimpleMessageBox (Just $ window) SDL.Information "Congratulations!" (Data.Text.pack("New highscore: " ++ (show $ currentPoints) ++ " points!.\nLast highscore was " ++ (show highscore)))
            saveScore currentPoints
          else do
            SDL.showSimpleMessageBox (Just $ window) SDL.Information "Game over" (Data.Text.pack("You have scored " ++ (show $ currentPoints) ++ " points.\nCurrent highscore is " ++ (show highscore)))
      else return ()
    return ()
      where
        currentPoints = gsPoints gs

fillRect :: SDL.Renderer -> Double -> Double -> Double -> Double -> IO ()
fillRect rend left top width height = 
    SDL.fillRect rend (Just (SDL.Rectangle (SDL.P $ SDL.V2 cleft ctop) (SDL.V2 cwidth cheight)))
    where
      trans = fromIntegral . round
      cleft = trans left
      ctop = trans top
      cwidth = trans width
      cheight = trans height


fillCircle :: SDL.Renderer -> Double -> Double -> Double -> IO ()
fillCircle rend x y r = do
  SDL.fillRect rend $ Just verticalRect
  SDL.fillRect rend $ Just horizontalRect 
  where
    trans = fromIntegral . round
    sin60 = sin (pi/3)
    cos60 = cos (pi/3)
    vertLeftTop = SDL.V2 (trans $ x - (cos60 * r)) (trans $ y-(sin60 * r))
    vertDims = SDL.V2 (trans r) (trans $ 2*r) 
    horizLeftTop = SDL.V2 (trans $ x - (sin60 * r)) (trans $ y-(cos60 * r))
    horizDims = SDL.V2 (trans $ 2*r) (trans r) 
    verticalRect = SDL.Rectangle (SDL.P vertLeftTop) vertDims 
    horizontalRect = SDL.Rectangle (SDL.P horizLeftTop) horizDims 

renderPhysicalObject :: SDL.Renderer -> PhysicalObject -> IO ()
renderPhysicalObject rend (PhysicalObject pos@(L.V2 x y) _ _ (Circle r) _ _ _ _ _) =
  fillCircle rend x y r
renderPhysicalObject rend (PhysicalObject pos@(L.V2 x y) _ _ (Rectangle (L.V2 wdth hght)) _ _ _  _ _) = 
  fillRect rend x y wdth hght

physicalObjectToRect :: PhysicalObject -> SDL.Rectangle CInt
physicalObjectToRect (PhysicalObject pos@(L.V2 x y) _ _ (Rectangle (L.V2 wdth hght)) _ _ _  _ _) = 
  SDL.Rectangle (SDL.P $ SDL.V2 cleft ctop) (SDL.V2 cwidth cheight)
    where
      trans = fromIntegral . round
      cleft = trans x
      ctop = trans y
      cwidth = trans wdth
      cheight = trans hght
physicalObjectToRect (PhysicalObject pos@(L.V2 x y) _ _ (Circle r) _ _ _ _ _) = 
  undefined

renderGameObject :: SDL.Renderer -> Textures -> GameState -> ObjId -> IO ()
renderGameObject rend textures gs objId = 
  SDL.copy rend targetTexture Nothing $ Just (physicalObjectToRect po)
    where
      ps = getPhysicalSystem gs
      go = (gsObjects gs) IntMap.! objId
      po = (psObjects ps) IntMap.! objId
      (L.V2 x y) = poPos po
      targetTexture = case go of
        Player -> textures Map.! PlayerTexture
        Obstacle -> if y < 300
          then textures Map.! ObstacleTopTexture
          else textures Map.! ObstacleBotTexture
        SolidWall -> if y < 0
          then textures Map.! SolidWallTopTexture
          else textures Map.! SolidWallBotTexture
        _ -> textures Map.! SolidWallTopTexture

      


